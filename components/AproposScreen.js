import React, {useEffect, useState} from 'react';
import { StyleSheet, View, Text, FlatList, Image, Pressable, Button } from 'react-native';

const AproposScreen = ({ navigation }) => {
    
    return (
      
      <View 
        style={ styles.container }
      >
        <Text style={styles.titre}>A propos</Text>
        <View>
          <Text>
            Cette application est basée sur l'API TheCocktailDB.
            Elle vous propose de découvrir une sélection aléatoire de 10 cocktails.
          </Text>
          <Text>Découvrez de nouvelles recette de cocktail pour égayer vos soirée ou vos apéros entre amis !</Text>
          <Text>Pour plus d'information, rendez vous sur:</Text>
        </View>
        <Text>www.thecocktaildb.com</Text>
        
        <View style={styles.bottomBar}>
          <Button title="Home" onPress={() => navigation.navigate('Home')}/>
          <Pressable onPress={() => navigation.navigate('A propos')}>
            <Text style={styles.cat}>A propos</Text>
          </Pressable>
        </View>
      </View>
    );
}

  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titre: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 50,
    marginBottom: 5,
  },
  image: {
    margin: 10,
    height: 100,
    width: 100,
  },
  cocktail: {
    alignItems: 'center',
    borderColor: "black",
    borderWidth: 3,
    margin: 5,
    width: 150,
    height: 150,
  },
  bottomBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50,
    width: 350,
    borderWidth: 1,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    paddingLeft: 10,
    marginTop: 400,
  },
  cat: {
    fontWeight: 'bold',
    marginLeft: 200,
    textDecorationLine: "underline"
  }
});

export default AproposScreen;
  