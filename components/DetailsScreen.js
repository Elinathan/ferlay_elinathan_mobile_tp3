import React, {useEffect, useState} from 'react';
import { StyleSheet, Button, View, Text, FlatList, Image, Pressable } from 'react-native';

const DetailsScreen = ({ route, navigation }) => {
    const { itemId } = route.params;
    const [detailsBoisson, setDetailsBoisson] = useState({});
    const [listeIgredients, setListeIgredients] = useState([]);

    useEffect(() => {
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${itemId}`)
        .then(response => response.json())
        .then(json => {
            setDetailsBoisson({"nom": json.drinks[0].strDrink, "image": json.drinks[0].strDrinkThumb, "recette": json.drinks[0].strInstructions,});
            if (json.drinks[0].strIngredient1 != null) { listeIgredients.push(json.drinks[0].strIngredient1)}
            if (json.drinks[0].strIngredient2 != null) { listeIgredients.push(json.drinks[0].strIngredient2)}
            if (json.drinks[0].strIngredient3 != null) { listeIgredients.push(json.drinks[0].strIngredient3)}
            if (json.drinks[0].strIngredient4 != null) { listeIgredients.push(json.drinks[0].strIngredient4)}
            if (json.drinks[0].strIngredient5 != null) { listeIgredients.push(json.drinks[0].strIngredient5)}
            if (json.drinks[0].strIngredient6 != null) { listeIgredients.push(json.drinks[0].strIngredient6)}
            if (json.drinks[0].strIngredient7 != null) { listeIgredients.push(json.drinks[0].strIngredient7)}
            if (json.drinks[0].strIngredient8 != null) { listeIgredients.push(json.drinks[0].strIngredient8)}
            if (json.drinks[0].strIngredient9 != null) { listeIgredients.push(json.drinks[0].strIngredient9)}
            if (json.drinks[0].strIngredient10 != null) { listeIgredients.push(json.drinks[0].strIngredient10)}
            if (json.drinks[0].strIngredient11 != null) { listeIgredients.push(json.drinks[0].strIngredient11)}
            if (json.drinks[0].strIngredient12 != null) { listeIgredients.push(json.drinks[0].strIngredient12)}
            if (json.drinks[0].strIngredient13 != null) { listeIgredients.push(json.drinks[0].strIngredient13)}
            if (json.drinks[0].strIngredient14 != null) { listeIgredients.push(json.drinks[0].strIngredient14)}
            if (json.drinks[0].strIngredient15 != null) { listeIgredients.push(json.drinks[0].strIngredient15)}
        }).catch(error => {
            console.error(error);
        })  
    }, []);
  
    const renderItem = ({item}) => {
        return (
            <View style={styles.row}>
                <Image style= {styles.imageIngredient} source={{ uri: `https://www.thecocktaildb.com/images/ingredients/${item}-Small.png` }}/>
                <Text>{item}</Text>
            </View>
        );
      };
  

    return (
      <View style={styles.container}>
        <Text style={styles.titre}>{detailsBoisson.nom}</Text>
        <Image style= {styles.image} source={{ uri: detailsBoisson.image }}/>
        <Text>{detailsBoisson.recette}</Text>
        <Text style={styles.petitTitre}>Ingredient: </Text>
        <FlatList
          style={styles.list}
          data={listeIgredients}
          renderItem={renderItem}
        />
        <View style={styles.button}>
          <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
        </View>

        <View style={styles.bottomBar}>
          <Button title="Home" onPress={() => navigation.navigate('Home')}/>
          <Pressable onPress={() => navigation.navigate('A propos')}>
            <Text style={styles.cat}>A propos</Text>
          </Pressable>
        </View>
      </View>
    );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  list: {
    height: 100,
  },
  titre: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
    marginBottom: 5,
  },
  petitTitre: {
    fontSize: 15,
    fontWeight: 'bold',
    marginTop: 5,
  },
  image: {
    margin: 10,
    height: 100,
    width: 100,
  },
  imageIngredient: {
    height: 20,
    width: 20,
  },
  row: {
    flexDirection: 'row',
  },
  button: {
    margin: 25,
  },
  bottomBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50,
    width: 350,
    borderWidth: 1,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    paddingLeft: 10,
  },
  cat: {
    fontWeight: 'bold',
    marginLeft: 200,
    textDecorationLine: "underline"
  }
});

export default DetailsScreen;