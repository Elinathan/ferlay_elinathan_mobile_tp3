import React, {useEffect, useState} from 'react';
import { StyleSheet, View, Text, FlatList, Image, Pressable, Button } from 'react-native';

const HomeScreen = ({ navigation }) => {
    const  [listeBoisson, setListeBoisson] = useState([])
    useEffect(() => {
      for (let pas = 0; pas < 10; pas++) {
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/random.php`)
        .then(response => response.json())
        .then(json => {
          listeBoisson.push({"ID": json.drinks[0].idDrink,"nom": json.drinks[0].strDrink, "image": json.drinks[0].strDrinkThumb})
        }).catch(error => {
          console.error(error);
        })  
      }
    }, []);
  
    const Item = ({itemId, title, image}) => (
      <View style= {styles.cocktail}>
        <Pressable 
          onPress={() => {
            navigation.navigate('Details', {
              itemId: itemId,
            });
          }}
        >
          <Text>{title}</Text>
          <Image style= {styles.image} source={{ uri: image }}/>
        </Pressable>
      </View>
    );
  
    const renderItem = ({item}) => {
      return (
        <Item
          itemId = {item.ID}
          title={item.nom}
          image={item.image}
        />
      );
    };
    
    return (
      
      <View 
        style={styles.container}
      >
        <Text style= {styles.titre}>Cocktails</Text>
        <FlatList
          data={listeBoisson}
          renderItem={renderItem}
        />
        
        <View style={styles.bottomBar}>
          <Button title="Home" onPress={() => navigation.navigate('Home')}/>
          <Pressable onPress={() => navigation.navigate('A propos')}>
            <Text style={styles.cat}>A propos</Text>
          </Pressable>
        </View>
      </View>
    );
}

  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titre: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
    marginBottom: 5,
  },
  image: {
    margin: 10,
    height: 100,
    width: 100,
  },
  cocktail: {
    alignItems: 'center',
    borderColor: "black",
    borderWidth: 3,
    margin: 5,
    width: 150,
    height: 150,
  },
  bottomBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50,
    width: 350,
    borderWidth: 1,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    paddingLeft: 10,
  },
  cat: {
    fontWeight: 'bold',
    marginLeft: 200,
    textDecorationLine: "underline"
  }
});

export default HomeScreen;
  